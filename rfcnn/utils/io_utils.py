import pickle
import pdb
import theano.tensor as T
import numpy as np

def load_weights(net, weights_path):
    """TODO: Docstring for load_weights.
    :returns: TODO
    """
    f = open(weights_path, 'rb')
    params = pickle.load(f)
    f.close()
    for k,v in params.items():
        if k in net.params:
            net.params[k].set_value(params[k].get_value())

def load_weights_cpu(net, weights_path):
    """TODO: Docstring for load_weights.
    :returns: TODO
    """
    f = open(weights_path, 'rb')
    params = pickle.load(f)
    f.close()
    for k,v in params.items():
        if k in net.params:
            net.params[k].set_value(params[k])

def copy_caffe_weights(net, path_to_caffemodel, path_to_prototxt):
    import caffe
    caffe_net= caffe.Net(path_to_prototxt, path_to_caffemodel, caffe.TEST)
    for layer in caffe_net.params:
        if layer+'_W' in net.params:
            net.params[layer+'_W'].set_value(caffe_net.params[layer][0].data)
        if layer+'_b' in net.params:
            net.params[layer+'_b'].set_value(caffe_net.params[layer][1].data)

def copy_caffe_dilation_weights(net, path_to_caffemodel, path_to_prototxt):
    import caffe
    caffe_net= caffe.Net(path_to_prototxt, path_to_caffemodel, caffe.TEST)
    for layer in caffe_net.params:
        if layer == 'ctx_upsample':
            all_ws= net.params[layer+'_W'].get_value()
            for i in range(all_ws.shape[1]):
                all_ws[:,i,:,:]= caffe_net.params[layer][0].data[:,0,:,:]
            net.params[layer+'_W'].set_value(all_ws)
        else:
            if layer+'_W' in net.params:
                net.params[layer+'_W'].set_value(caffe_net.params[layer][0].data)
            if layer+'_b' in net.params:
                net.params[layer+'_b'].set_value(caffe_net.params[layer][1].data)
    pdb.set_trace()

def save_net_weights(net, net_save_path):
    """TODO"""
    print ("saving the network \'%s\' into %s"%(net.name, net_save_path))
    f = open(net_save_path, 'wb')
    pickle.dump(net.params, f, protocol=pickle.HIGHEST_PROTOCOL)
    f.close()


