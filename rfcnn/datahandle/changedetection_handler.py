import numpy as np
from threading import Thread
import h5py
from theano import config
from ..utils import numpy_floatX, sliding_window
from .datahandler import DataHandler
import pdb
import matplotlib.pyplot as plt

class CHDHandler(DataHandler):
    """Docstring for CityScapes dataset. """
    def __init__(self, file_path,
                 train_batchsize=16, valid_batchsize=15,
                 test_batchsize=13,
                 normalize=False, n_frames=1):
        """
        """
        super(DataHandler, self).__init__()
        self.hf = h5py.File(file_path)
        self.n_frames=n_frames
        self.normalize= normalize
        self.is_temporal = self.n_frames > 1
        self.img_shape = self.hf['images_train'].shape[1:]
        self.label_shape = self.hf['labels_train'].shape[1:]
        self.n_train_samples = len(self.hf['names_train'][:])
        self.n_valid_samples = len(self.hf['names_valid'][:])
        self.n_test_samples = len(self.hf['names_test'][:])

        self.train_batchsize = train_batchsize
        self.valid_batchsize = valid_batchsize
        self.test_batchsize = test_batchsize

        def get_idx(length, batchsize,reshuffle=True):
            "function to generate ids for each batch without replacement"
            pointer = 0
            shuffled = np.random.permutation(length)
            while True:
                if pointer + batchsize <= len(shuffled):
                    yield shuffled[pointer:pointer+batchsize]
                    if pointer + batchsize == len(shuffled):
                        pointer = 0
                    else:
                        pointer = pointer + batchsize
                else:
                    diff = len(shuffled) - pointer
                    yield np.concatenate((shuffled[pointer:],
                                          shuffled[0:batchsize-diff]))
                    if reshuffle:
                        shuffled = np.random.permutation(length)
                    pointer = batchsize-diff

        self.train_gen_idx = get_idx(self.n_train_samples-self.n_frames,
                                     train_batchsize)
        self.valid_gen_idx = get_idx(self.n_valid_samples-self.n_frames,
                                     valid_batchsize)
        self.test_gen_idx = get_idx(self.n_test_samples-self.n_frames,
                                     test_batchsize)

    def getMiniBatch(self, split):
        if split not in ['train', 'valid', 'test']:
            raise NameError("partion %s is not included in the dataset"%split)
        if split == 'train':
            n_samples = self.n_train_samples
            batchsize = self.train_batchsize
            gen = self.train_gen_idx
        if split == 'valid':
            n_samples = self.n_valid_samples
            batchsize = self.valid_batchsize
            gen = self.valid_gen_idx
        if split == 'test':
            n_samples = self.n_test_samples
            batchsize = self.test_batchsize
            gen = self.test_gen_idx
        n_samples = n_samples - self.n_frames + 1


        if self.is_temporal:
            X = np.zeros((batchsize, self.n_frames, self.img_shape[0],
                          self.img_shape[1]), dtype= config.floatX)
            Y = np.zeros((batchsize,
                          self.label_shape[0]), dtype= 'int64')
        else:
            X = np.zeros((batchsize, self.img_shape[0],
                          self.img_shape[1]), dtype= config.floatX)
            Y = np.zeros((batchsize,
                          self.label_shape[0]), dtype= 'int64')

        idx = next(gen)
        names=[]
        correct_index= 0
        for i in range(len(idx)):
            names= self.hf['names_'+split][idx[i]:idx[i]+self.n_frames]
            if names[0].split('/')[-3] != names[-1].split('/')[-3]: #Reached end of dataset
                X[i] = np.asarray(self.hf['images_'+split][correct_index:correct_index+self.n_frames], dtype= config.floatX)
                if split is not 'test':
                    Y[i] = np.asarray(self.hf['labels_'+split][correct_index+self.n_frames], dtype= 'int64')
            else:
                X[i] = np.asarray(self.hf['images_'+split][idx[i]:idx[i]+self.n_frames], dtype= config.floatX)
                if split is not 'test':
                    Y[i] = np.asarray(self.hf['labels_'+split][idx[i]+self.n_frames], dtype= 'int64')
                correct_index= idx[i]
        return X, Y


