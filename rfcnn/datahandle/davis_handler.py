import numpy as np
from threading import Thread
import h5py

from ..utils import numpy_floatX, sliding_window
from .datahandler import DataHandler

import pdb

class DavisDataHandler(DataHandler):
    """Docstring for DAVIS dataset. """
    def __init__(self, root_path, datainfo = 'default', update_train=True,
                 load_valid=True, load_test=False, temporal=False,
                 n_frames=None, normalize= False):
        """
        """
        super(DavisDataHandler, self).__init__()
        self.n_frames=n_frames
        self.temporal = temporal
        self.normalize= normalize
        self.root_path = root_path
        self.datainfo = []
        self.prev_train = None
        if datainfo=='default':
            self.default_datainfo()
        else:
            self.create_datainfo(datainfo)

        self.current_train = 0
        if load_valid:
            self.load_valid()
        if load_test:
            self.load_test()
        if update_train:
            self.update_train()
        else:
            self.load_train()

    def update_train_index(self, update_to=None):
        if update_to != None:
            self.current_train = update_to
        else:
            self.current_train += 1
            if self.current_train == len(self.datainfo):
                self.current_train = 0

    def create_datainfo(self, data_info):
        """
        data_info is list of seqs names
        """
        root_path = self.root_path
        for seq in data_info:
            self.append_datainfo(root_path+seq, data_type='h5py')


    def default_datainfo(self):
        """
        """
        root_path = self.root_path
        self.append_datainfo(root_path+'bear', data_type='h5py')
        self.append_datainfo(root_path+'blackswan', data_type='h5py')
        self.append_datainfo(root_path+'bmx-bumps', data_type='h5py')
        self.append_datainfo(root_path+'bmx-trees', data_type='h5py')
        self.append_datainfo(root_path+'boat', data_type='h5py')
        self.append_datainfo(root_path+'breakdance', data_type='h5py')
        self.append_datainfo(root_path+'breakdance-flare', data_type='h5py')
        self.append_datainfo(root_path+'bus', data_type='h5py')
        self.append_datainfo(root_path+'camel', data_type='h5py')
        self.append_datainfo(root_path+'car-roundabout', data_type='h5py')
        self.append_datainfo(root_path+'car-shadow', data_type='h5py')
        self.append_datainfo(root_path+'car-turn', data_type='h5py')
        self.append_datainfo(root_path+'cows', data_type='h5py')
        self.append_datainfo(root_path+'dance-jump', data_type='h5py')
        self.append_datainfo(root_path+'dance-twirl', data_type='h5py')
        self.append_datainfo(root_path+'dog', data_type='h5py')
        self.append_datainfo(root_path+'dog-agility', data_type='h5py')
        self.append_datainfo(root_path+'drift-chicane', data_type='h5py')
        self.append_datainfo(root_path+'drift-straight', data_type='h5py')
        self.append_datainfo(root_path+'drift-turn', data_type='h5py')
        self.append_datainfo(root_path+'elephant', data_type='h5py')
        self.append_datainfo(root_path+'flamingo', data_type='h5py')
        self.append_datainfo(root_path+'goat', data_type='h5py')
        self.append_datainfo(root_path+'hike', data_type='h5py')
        self.append_datainfo(root_path+'hockey', data_type='h5py')
        self.append_datainfo(root_path+'horsejump-high', data_type='h5py')
        self.append_datainfo(root_path+'horsejump-low', data_type='h5py')
        self.append_datainfo(root_path+'kite-surf', data_type='h5py')
        self.append_datainfo(root_path+'libby', data_type='h5py')
        self.append_datainfo(root_path+'lucia', data_type='h5py')
        self.append_datainfo(root_path+'mallard-fly', data_type='h5py')
        self.append_datainfo(root_path+'mallard-water', data_type='h5py')
        self.append_datainfo(root_path+'motocross-bumps', data_type='h5py')
        self.append_datainfo(root_path+'motocross-jump', data_type='h5py')
        self.append_datainfo(root_path+'paragliding', data_type='h5py')
        self.append_datainfo(root_path+'paragliding-launch', data_type='h5py')
        self.append_datainfo(root_path+'parkour', data_type='h5py')
        self.append_datainfo(root_path+'rhino', data_type='h5py')
        self.append_datainfo(root_path+'rollerblade', data_type='h5py')
        self.append_datainfo(root_path+'scooter-black', data_type='h5py')
        self.append_datainfo(root_path+'scooter-gray', data_type='h5py')
        self.append_datainfo(root_path+'soapbox', data_type='h5py')
        self.append_datainfo(root_path+'soccerball', data_type='h5py')
        self.append_datainfo(root_path+'stroller', data_type='h5py')
        self.append_datainfo(root_path+'surf', data_type='h5py')
        self.append_datainfo(root_path+'swing', data_type='h5py')
        self.append_datainfo(root_path+'tennis', data_type='h5py')
        self.append_datainfo(root_path+'train', data_type='h5py')

    def append_datainfo(self, path, data_type=None, all_train=False, shape=None):
        """TODO: Docstring for data_append.
        :returns: TODO
        """
        if data_type == None:
            data_type=path.split('.')[-1]
        self.datainfo.append({'path':path,
                              'data_type': data_type,
                              'all_train': all_train,
                              'shape': shape})

    def _load_chunk(self, subset_info):
        """
        TODO
        """
        train_x=None
        train_y=None
        valid_x=None
        valid_y=None
        test_x=None
        test_y=None

        if subset_info['data_type']=='h5py':
            hf = h5py.File(subset_info['path'], 'r')
            if subset_info['all_train']:
                train_x = np.concatenate((np.array(hf.get('images_train')),
                                          np.array(hf.get('images_valid')),
                                          np.array(hf.get('images_test'))),
                                          axis=0)
                train_y = np.concatenate((np.array(hf.get('labels_train')),
                                          np.array(hf.get('labels_valid')),
                                          np.array(hf.get('labels_test'))),
                                          axis=0)
            else:
                train_x = np.array(hf.get('images_train'))
                train_y = np.array(hf.get('labels_train'))
                valid_x = np.array(hf.get('images_valid'))
                valid_y = np.array(hf.get('labels_valid'))
                test_x = np.array(hf.get('images_test'))
                test_y = np.array(hf.get('labels_test'))
            hf.close()

            return train_x, train_y, valid_x, valid_y, test_x, test_y

        else:
            raise NotImplementedError('the loading method %s is not implemented'%(
                                      subset_info['data_type']))

    def update_train(self, randomize=True):
        if self.prev_train == self.current_train:
            return
        subset = self.datainfo[self.current_train]
        self.prev_train = self.current_train
        for i in range(len(self.datainfo)):
            x,y,_,_,_,_=self._load_chunk(subset)
            if x is not None:
                break

        if self.temporal:
            x, y= sliding_window(x, y, self.n_frames)
            y = np.ndarray.astype(y[:,-1,:]>0,np.int64)

        if randomize:
            tr_ind= np.random.permutation(x.shape[0])
            x = x[tr_ind, :]
            y = y[tr_ind, :]

        if self.normalize:
            x= x/255.0

        self.train_x = numpy_floatX(x)
        self.train_y = np.asarray(y, dtype='int64')

    def load_train(self, randomize=True):
        self.train_x = None
        self.train_y = None
        for subset in self.datainfo:
            x,y,_,_,_,_=self._load_chunk(subset)
            if x is not None and y is not None:
                if self.train_x is None:
                    if self.temporal:
                        self.train_x, self.train_y= sliding_window(x, y, self.n_frames)
                        self.train_y = np.ndarray.astype(self.train_y[:,-1,:]>0,np.int64)
                    else:
                        self.train_x = x
                        self.train_y = y
                else:
                    if self.temporal:
                        temp_x, temp_y= sliding_window(x, y, self.n_frames)
                        temp_y = np.ndarray.astype(temp_y[:,-1,:]>0,np.int64)
                        self.train_x = np.concatenate((self.train_x,
                                                      temp_x), axis=0)
                        self.train_y = np.concatenate((self.train_y,
                                                      temp_y), axis=0)
                    else:
                        self.train_x = np.concatenate((self.train_x,
                                                      x), axis=0)
                        self.train_y = np.concatenate((self.train_y,
                                                      y), axis=0)

        if self.normalize:
            self.train_x= self.train_x/255.0

        if randomize:
            tr_ind= np.random.permutation(self.train_x.shape[0])
            self.train_x = self.train_x[tr_ind, :]
            self.train_y = self.train_y[tr_ind, :]

        self.train_x = numpy_floatX(self.train_x)
        self.train_y = np.asarray(self.train_y, dtype='int64')


    def load_valid(self):
        self.valid_x = None
        self.valid_y = None
        for subset in self.datainfo:
            _,_,x,y,_,_=self._load_chunk(subset)
            if x is not None and y is not None:
                if self.valid_x is None:
                    self.valid_x = x
                    self.valid_y = y
                else:
                    self.valid_x = np.concatenate((self.valid_x,
                                                  x), axis=0)
                    self.valid_y = np.concatenate((self.valid_y,
                                                  y), axis=0)
        if self.temporal:
            self.valid_x, self.valid_y= sliding_window(self.valid_x,
                                                       self.valid_y,
                                                       self.n_frames)
            self.valid_y = np.ndarray.astype(self.valid_y[:,-1,:]>0,np.int64)

        if self.normalize:
            self.valid_x= self.valid_x/255.0

        self.valid_x = numpy_floatX(self.valid_x)
        self.valid_y = np.asarray(self.valid_y, dtype='int64')

    def load_test(self):
        self.test_x = None
        self.test_y = None
        for subset in self.datainfo:
            _,_,_,_,x,y=self._load_chunk(subset)
            if x!=None and y!=None:
                if self.test_x==None:
                    self.test_x = x
                    self.test_y = y
                else:
                    self.test_x = np.concatenate((self.test_x,
                                                  x), axis=0)
                    self.test_y = np.concatenate((self.test_y,
                                                  y), axis=0)
        if self.temporal:
            self.test_x, self.test_y= sliding_window(self.test_x,
                                                     self.test_y,
                                                     self.n_frames)
            self.test_y = np.ndarray.astype(self.test_y[:,-1,:]>0,np.int64)

        if self.normalize:
            self.test_x= self.test_x/255.0

        self.test_x = numpy_floatX(self.test_x)#/255.0)
        self.test_y = np.asarray(self.test_y, dtype='int64')




