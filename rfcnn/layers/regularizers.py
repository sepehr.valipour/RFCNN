import numpy
import theano
import theano.tensor as T
from theano import config
from ..utils.generic import numpy_floatX

from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams

class SpatialDropout(object):
    """
    Spatial Dropout Layer, multiplies the mask by the an entire feature map
    """

    def __init__(self, name, prob= 0.5, phase= 'TRAIN'):
        """
        prob: probability by which the feature maps are dropped
        """
        self.prob= prob
        self.name= name
        self._srng= RandomStreams()
        self.phase= phase

    def __call__(self, layer_input):
        """
        layer_input: input to the layer that needs to be masked
        returns output of dropout after masking
        mask is with size : batchsize*nfeatures then it's broadcasted to the last 2 dims
        """
        if self.phase == 'TRAIN':
            self.mask= self._srng.binomial(layer_input.shape[:2], p= (1-self.prob), dtype= config.floatX)
            axes= [0,1]+(['x']* (layer_input.ndim-2) )
            self.mask= self.mask.dimshuffle(*axes)
            output= layer_input*self.mask
        else:
            output= (1-self.prob)*layer_input

        return output

    def get_shape(self, input_shape):
        """ return shape of the output """
        return input_shape
