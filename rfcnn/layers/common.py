import numpy
import theano
from theano.tensor.signal import pool
import theano.tensor as T
from theano import config

import pdb

from ..utils.generic import numpy_floatX

class Pooling(object):

    """Docstring for convolution. """

    def __init__(self, name, ds=(2,2), st=(2,2), ignore_border=True):
        """TODO: to be defined1. """
        self.name = name
        self.ds = ds
        self.st = st
        self.ignore_border = ignore_border


    def __call__(self, layer_input):
        """TODO: Docstring for __call__.
        :returns: TODO
        """
#        return downsample.max_pool_2d(layer_input, self.ds, st=self.st, ignore_border=self.ignore_border)
	return pool.pool_2d(layer_input, self.ds, st= self.st, ignore_border= self.ignore_border, mode= 'max')

    def get_shape(self, input_shape):
        """
        TODO
        """
        ds = self.ds
        st = self.st
        return [input_shape[0],
                (input_shape[1] - ds[0])//st[0] + 1,
                (input_shape[2] - ds[1])//st[1] + 1]


class Relu(object):

    """Docstring for convolution. """

    def __init__(self, name):
        """TODO: to be defined1. """
        self.name = name

    def __call__(self, layer_input):
        """TODO: Docstring for __call__.
        :returns: TODO
        """
        return T.nnet.relu(layer_input)

    def get_shape(self, input_shape):
        """
        TODO
        """
        return input_shape


class Sigmoid(object):

    """Docstring for convolution. """

    def __init__(self, name):
        """TODO: to be defined1. """
        self.name = name

    def __call__(self, layer_input):
        """TODO: Docstring for __call__.
        :returns: TODO
        """
        return T.nnet.sigmoid(layer_input)

    def get_shape(self, input_shape):
        """
        TODO
        """
        return input_shape


class Softmax(object):

    """Docstring for convolution. """

    def __init__(self, name):
        """TODO: to be defined1. """
        self.name = name

    def __call__(self, layer_input):
        """TODO: Docstring for __call__.
        :returns: TODO
        """
        shp_preds=layer_input.shape
        layer_input = layer_input.dimshuffle((0,2,3,1))
        layer_input = layer_input.reshape((shp_preds[0]*shp_preds[2]*shp_preds[3], shp_preds[1]))
        return T.nnet.softmax(layer_input)

    def get_shape(self, input_shape):
        """
        TODO
        """
        return (input_shape[1]*input_shape[2], input_shape[0])


class NLL(object):
    """Docstring for convolution. """
    def __init__(self, name, label_type):
        """TODO: to be defined1. """
        self.name = name
        self.label_type = label_type

    def __call__(self, layer_input, label_input):
        """TODO: Docstring for __call__.
        :returns: TODO
        """
        if self.label_type=='classification': # The regular negative log likelihood for multiclass
            return -T.mean(T.log(layer_input)[T.arange(label_input.shape[0]), label_input])

        else: # Negative log likelihood for two classes and on segmentation output
            label_input = label_input.flatten()
            return T.mean(T.nnet.categorical_crossentropy(layer_input, label_input))

    def get_shape(self, input_shape):
        """
        TODO
        """
        return (1)

class NLLWeighted(object):
    """Docstring for convolution. """
    def __init__(self, name, label_type, num_classes):
        """TODO: to be defined1. """
        self.name = name
        self.label_type = label_type
        self.num_classes = num_classes

    def __call__(self, layer_input, label_input):
        """TODO: Docstring for __call__.
        :returns: TODO
        """
        label_input = label_input.flatten()
        # label_input shape: (batch_size * image_size)
        # layer_input shape: (batch_size * image_size) by #-classes
        # size: number_of_pix by 1
        loss_per_pix = T.nnet.categorical_crossentropy(layer_input, label_input)
        # number of bins is one larger than the largest number in x so it's 5, corresonpding to [0,1,2,3,4]
        bins = theano.tensor.extra_ops.bincount(label_input, minlength=self.num_classes)
        # compute the weight of the loss inverse-proportional to the class weight
        eps = 100 # prevent from dividing by zero
        # size: 5, inverse of the number of elements
        weights_per_class = T.inv(bins+eps) #* label_input.shape[0]
        weights_per_class = weights_per_class / T.sum(weights_per_class)
        weights_per_pixel = T.ones(loss_per_pix.shape)
        for idx_class in range(self.num_classes):
            weights_per_pixel = T.switch(T.eq(label_input,T.constant(idx_class)), weights_per_class[idx_class], weights_per_pixel)

        return T.dot(loss_per_pix , weights_per_pixel)

    def get_shape(self, input_shape):
        """
        TODO
        """
        return (1)

class NLLFixWeighted(object):
    """Docstring for convolution. """
    def __init__(self, name, label_type, weights=numpy.array([0.0004, 0.5431, 0.0473, 0.2326, 0.1766])):
        """TODO: to be defined1. """
        self.name = name
        self.label_type = label_type
        self.weights = weights

    def __call__(self, layer_input, label_input):
        """TODO: Docstring for __call__.
        :returns: TODO
        """
        label_input = label_input.flatten()
        # label_input shape: (batch_size * image_size)
        # layer_input shape: (batch_size * image_size) by #-classes
        # size: number_of_pix by 1
        loss_per_pix = T.nnet.categorical_crossentropy(layer_input, label_input)
        # compute the weight of the loss inverse-proportional to the class weight
        # size: 5, inverse of the number of elements
        # 0.0004+0.5431+0.0473+0.2326+0.1766 = 1
        # find the items in each class from label_input
        weights_per_pixel = T.ones(loss_per_pix.shape)
        weights_per_pixel = T.switch(T.eq(label_input,T.constant(0)), self.weights[0], weights_per_pixel)
        weights_per_pixel = T.switch(T.eq(label_input,T.constant(1)), self.weights[1], weights_per_pixel)
        weights_per_pixel = T.switch(T.eq(label_input,T.constant(2)), self.weights[2], weights_per_pixel)
        weights_per_pixel = T.switch(T.eq(label_input,T.constant(3)), self.weights[3], weights_per_pixel)
        weights_per_pixel = T.switch(T.eq(label_input,T.constant(4)), self.weights[4], weights_per_pixel)

        return T.dot(loss_per_pix , weights_per_pixel)

    def get_shape(self, input_shape):
        """
        TODO
        """
        return (1)

class LRN(object):

    """Docstring for convolution. """

    def __init__(self, name, alpha=5e-4, k=2, beta=0.75, n=5):
        """TODO: to be defined1. """
        self.name = name
        self.alpha = alpha
        self.k = k
        self.beta = beta
        self.n = n

    def __call__(self, layer_input):
        """TODO: Docstring for __call__.
        :returns: TODO
        """
        half = self.n // 2

        sq = T.sqr(layer_input)
        sq = T.transpose(sq,(1,2,3,0))
        ch, h, w, b = sq.shape

        extra_channels = T.alloc(0.,ch + 2*half, h, w, b)
        sq = T.set_subtensor(extra_channels[half:half+ch,:,:,:], sq)
        scale = self.k
        for i in xrange(self.n):
            scale += self.alpha * sq[i:i+ch,:,:,:]
        scale = scale ** self.beta
        scale = T.transpose(scale,(3,0,1,2))
        return layer_input/ scale

    def get_shape(self, input_shape):
       """
       TODO
       """
       return input_shape



class Data(object):
    """Docstring for convolution. """

    def __init__(self, name, input_shape, reshape=None, dimshuffle=None, flatten=None, mean_subtraction = None):
        """TODO: to be defined1. """
        self.name = name
        self.reshape = reshape
        self.input_shape = input_shape
        self.dimshuffle = dimshuffle
        self.flatten  = flatten
        self.mean_subtraction = mean_subtraction

    def __call__(self, layer_input):
        """TODO: Docstring for __call__.
        :returns: TODO
        """
        if self.mean_subtraction is not None:
            mean = numpy.zeros((self.input_shape[0],
                                self.input_shape[1]*self.input_shape[2]))
            for dim in range(self.input_shape[0]):
                mean[dim] = numpy.tile(self.mean_subtraction[dim],
                                    self.input_shape[1]*self.input_shape[2])

            layer_input = layer_input - mean
        if self.reshape is not None:
            if self.reshape is True:
                layer_input = layer_input.reshape([layer_input.shape[0]] + self.input_shape)
            else:
                layer_input = layer_input.reshape([layer_input.shape[0]] + self.reshape)

        if self.dimshuffle is not None:
            layer_input = layer_input.dimshuffle(self.dimshuffle)

        if self.flatten is not None:
            layer_input = layer_input.flatten()

        return layer_input

    def get_shape(self, input_shape):
       """
       TODO
       """
       out_shape = self.input_shape
       #if self.reshape is not None and is not True:
       #    out_shape = self.reshape
       return out_shape



