from __future__ import print_function
import numpy
import time
import random

import pdb

from metrics import Metrics
from ..utils import io_utils
from theano import config

class Train(object):
    """
    Main training object.
    Handles training, evaluation, saving, logging
    """
    def __init__(self, net,
                X=None, Y=None, configs={}, datahandler=None,
                X_valid=None, Y_valid=None,
                X_test=None, Y_test=None):
        """
        Initializing Train object:
        X: data subset that the network is going to train on.
        Y: labels for X
        configs: configs for training. if nothing is passed the default will be used.
                 defaults can be seen below.
                 args passed will override or append the default configs.
        X_valid: data subset for validation
        Y_valid: labels for X_valid
        X_test: data subset for test
        Y_test: labels for X_test
        """
        self.datahandler = datahandler
        self.net = net
        self.configs = self.mod_configs(configs)
        if self.datahandler != None:
            self.X = self.datahandler.train_x
            self.Y = self.datahandler.train_y
        else:
            self.X = X
            self.Y = Y
        self.do_valid = False
        self.do_test = False
        if X_valid != None and Y_valid != None:
            self.do_valid = True
            self.X_valid = X_valid
            self.Y_valid = Y_valid
        if X_test != None and Y_test != None:
            self.do_test= True
            self.X_test = X_test
            self.Y_test = Y_test

        self.logs = self.configs.copy()
        self.logger = Logger(self.configs['log_file'])

    def mod_configs(self, configs):
        """
        append or override default configs with the given config.
        configs: configs for training. if nothing is passed the default will be used.
                 defaults can be seen below.
                 args passed will override or append the default configs.
        returns: modified default config
        """
        default_configs={
            'patience' : 80,  # Number of epoch to wait before early stop if no progress
            'patience_increase' : 1.3,
            'improvement_threshold' : 0.995,
            'max_epochs' : 500,  # The maximum number of epoch to run
            'valid_freq' : 10,  # Compute the validation error after this number of update.
            'save_freq' : 50,  # Save the parameters after every saveFreq updates
            'save_criteria':None, #if f_measure reaches this value the training stops
                                  # with the saved model
            'net_save_path': None,
            'train_batch_size':16,  # The batch size during training.
            'valid_batch_size':32,  # The batch size used for validation/test set.
            'test_batch_size':16,  # The batch size used for validation/test set.
            'epochs_per_datachunk':1, #Defines number of epochs that the training spends on ecach data chunk
            'log_file' : 'log.txt',
            }
        for k in default_configs:
            if k in configs:
                default_configs[k] = configs[k]
        return default_configs

    def train_data_updated(self, epoch):
        """
        Checks if it is time to update the data. If it is the time,
            it will update self.X, self.Y and returns True.
            Otherwise it returns False.
        epoch: current epoch number
        """
        if self.datahandler == None:
            return False
        if epoch!=0 and epoch%self.configs['epochs_per_datachunk']==0:
            if self.datahandler.data_ready:
                self.X = self.datahandler.train_x
                self.Y = self.datahandler.train_y

                self.datahandler.update_train_index()
                self.datahandler.request_update()
                return True
            else:
                return False

            #self.datahandler.update_train()
        else:
            return False

    def gen_interm_output(self, X):
        temp_out= self.interm_f(X)
        if self.interm_out is None:
            self.interm_out= temp_out
        else:
            self.interm_out= numpy.vstack((self.interm_out, temp_out))

    def eval(self, X, Y, batch_size=32, save_interm= None):
        """
        evaluation function for the network.
        X: data subset for evaluation
        Y: labels for X
        batch_size: the batch_size used for evaluation
        save_interm: pass name of layer if you want to save intermediate output for pretraining
        interm_out_path: path of the filename for the intermediate output
        returns: average cost of the data
                 precision
                 recall
                 F_measure
        """
        mini_batches = get_mini_batches(X.shape[0],
                                        batch_size,
                                        border_mode= 'repeat')
        self.net.set_phase('TEST')
        if save_interm is not None:
            self.interm_f= theano.function([self.net.x], self.net.comp_graphs[save_interm])

        self.interm_out= None
        met= Metrics(self.nclasses)

        for batch in mini_batches:
            preds = self.net.f_pred(X[batch])
            error = self.net.f_test(X[batch], Y[batch])
            cost= self.net.f_cost(X[batch], Y[batch])
            met.update_metrics(preds, Y[batch], error, cost)

            if save_interm is not None:
                self.gen_interm_output(X[batch])

        met.compute_final_metrics(len(mini_batches))
        self.net.set_phase('TRAIN')
        return met, self.interm_out

    def run_all(self):
        """
        runs the training over the whole self.X, self.Y.
        performs validation, testing, logging and saving based on the configs
        """

        self.logger.on_train_begin(self.logs)
        mini_batches = get_mini_batches(self.X.shape[0],
                                        self.configs['train_batch_size'],
                                        border_mode='repeat')
        self.best_validation = numpy.inf
        self.best_fmes = 0
        self.patience = self.configs['patience']
        for epoch in range(self.configs['max_epochs']):
            self.logs['epoch'] = epoch
            if self.train_data_updated(epoch):
                mini_batches = get_mini_batches(self.X.shape[0],
                                        self.configs['train_batch_size'],
                                        border_mode='repeat')

            for batch, iter in zip(mini_batches,range(len(mini_batches))):
                self.logs['iter'] = iter
                trainx = self.X[batch]
                trainy = self.Y[batch]
                cost_ij = self.net.f_forward_grad(trainx, trainy)
                self.net.f_update()
                self.logs['cost'] = cost_ij
                self.logger.on_batch_end(self.logs)

            if (epoch % self.configs['valid_freq'])==0 and self.do_valid:
                self.logs['valid_metrics'],_ = self.eval(self.X_valid, self.Y_valid, batch_size=self.configs['valid_batch_size'])
                self.logger.on_valid(self.logs)
                if self.best_validation > self.logs['valid_metrics'].cost:
                    self.best_validation = self.logs['valid_metrics'].cost
                if self.best_fmes < self.logs['valid_metrics'].fmes:
                    self.best_fmes = self.logs['valid_metrics'].fmes

            if (epoch % self.configs['save_freq'])==0 and \
                    self.configs['net_save_path']!=None and \
                    (self.best_validation >= self.logs['valid_metrics'].cost or \
                     self.best_fmes <= self.logs['valid_metrics'].fmes):
                io_utils.save_net_weights(self.net, self.configs['net_save_path'])

        if self.do_test:
            self.logs['test_metrics'], _= self.eval(self.X_test, self.Y_test, batch_size=self.configs['test_batch_size'])
            self.logger.on_test(self.logs)

        self.logger.on_train_end(self.logs)

    def earlystop(self, epoch):
        if self.logs['valid_metrics'].cost < self.best_validation:

            if self.logs['valid_metrics'].cost < self.best_validation*self.configs['improvement_threshold']:
                print('early stopping is moved to max of (%s , %s)'%(self.patience, epoch*self.configs['patience_increase']))
                self.patience= max(self.patience, epoch*self.configs['patience_increase'])

            self.best_validation= self.logs['valid_metrics'].cost
            self.logs['patience'] = self.patience
        if self.patience<= epoch: # early stopping criteria
            return True
        return False

def get_mini_batches(num_samples, batch_size, border_mode='repeat'):
    """
    creates indices for mini batches.
    num_samples: number of samples in the dataset
    batch_size: batch_size
    border_mode: if num_sample is not divisible by batch_size, border_mode handles the last mini_batch
                 "repeat": any shortage in the last mini_batch will be filled by random samples from the dataset
                 "depracate": ignores samples in the last mini_batch
    """
    n_batches = num_samples//batch_size
    mini_batches = []
    batch = []
    for i in range(num_samples):
        if len(batch) < batch_size:
            batch.append(i)
        else:
            mini_batches.append(batch)
            batch = [i]

    if len(batch) > 0:
        if border_mode == 'repeat':
            batch = batch + random.sample(range(num_samples), batch_size-len(batch))
            mini_batches.append(batch)
        if border_mode == 'deprecate':
            pass
    return mini_batches

class Logger(object):
    """
    Handles logging during the training.
    every function receives a dictionary (logs) containing all the
        information needed for logging.
    other functions can be added if necessary.
    """
    def __init__(self, log_file, write_mode='w'):
        """
        initilizes the log file.
        log_file: path to the file that the logs will be saved into
        write_mode: to create a new file or append an exisiting one.
        """
        self.log_file = log_file
        self.f = open(log_file, write_mode)

    def print_file_console(self, S=[], verbose=1):
        """
        printing util for logger. writes both on console and in a file based on verbose.
        S: a list of strings to be written on the file or console
        verbose: indicates to wirte on console or not
        """
        for line in S:
            self.f.write(line)
            if verbose == 1:
                print (line, end="")

    def on_train_begin(self, logs={}):
        """
        logging function to be called in an appropriate place on the main train loop.
        logs: a dictionary containing all the information needed for logging
        """
        self.start_time = time.time()
        S = []
        S.append("\n\n=======================================================\n")
        S.append("Starting to train at %s\n"%(time.asctime()))
        S.append("Number of epochs are: %s \n"%logs['max_epochs'])
        S.append("train batch size is: %s \n"%logs['train_batch_size'])
        S.append("logs are to be written to %s\n"%(self.log_file))
        S.append("trained model will be saved to %s\n"%(logs['net_save_path']))
        S.append("=======================================================\n\n")
        self.print_file_console(S)

    def on_train_end(self, logs={}):
        """
        logging function to be called in an appropriate place on the main train loop.
        logs: a dictionary containing all the information needed for logging
        """
        S = []
        S.append("training finished at %s after %s minutes"%(time.asctime(), (time.time()-self.start_time)/60))
        S.append("logs are to be written to %s"%(self.log_file))
        S.append("trained model will be saved to %s"%(logs['net_save_path']))
        self.print_file_console(S)

    def on_batch_end(self, logs={}):
        """
        logging function to be called in an appropriate place on the main train loop.
        logs: a dictionary containing all the information needed for logging
        """
        S = []
        S.append("cost at (%s)th iteration of epoch %s is %s\n"%(logs['iter'], logs['epoch'], logs['cost']))
        self.print_file_console(S)

    def on_valid(self, logs={}):
        """
        logging function to be called in an appropriate place on the main train loop.
        logs: a dictionary containing all the information needed for logging
        """
        S = []
        S.append("\nVALIDATION at epoch %s:\n"%(logs['epoch']))
        S.append("valid cost is: %s\n"%(logs['valid_metrics'].cost))
        S.append("valid precision is: %s \n"%(logs['valid_metrics'].prec ))
        S.append("valid recall is: %s \n"%(logs['valid_metrics'].rec))
        S.append("valid F_measure is: %s \n"%(logs['valid_metrics'].fmes))
        S.append("Patience now is: %s \n"%(logs['patience']))
        S.append("valid IOU is: %s \n"%(logs['valid_metrics'].mean_iou_index))
        self.print_file_console(S)

    def on_test(self, logs={}):
        """
        logging function to be called in an appropriate place on the main train loop.
        logs: a dictionary containing all the information needed for logging
        """
        S = []
        S.append("\n TEST at epoch %s:\n"%(logs['epoch']))
        S.append("test cost is: %s \n"%(logs['test_metrics'].cost))
        S.append("test precision is: %s \n"%(logs['test_metrics'].prec))
        S.append("test recall is: %s \n"%(logs['test_metrics'].rec))
        S.append("test F_measure is: %s \n"%(logs['test_metrics'].fmes))
        self.print_file_console(S)


