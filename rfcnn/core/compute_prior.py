import numpy as np
import sys
import h5py
import matplotlib.pyplot as plt
import pdb
from scipy import ndimage

def read_traindata(filepath):
    hf= h5py.File(filepath)
    labels= np.array(hf['labels_train'])
    return labels

def normalize(arr, ncs):
    norm = [float(i)/(sum(arr)*ncs) for i in arr]
    return norm

def compute_prior(labels, nclasses, shape):
    total_maps= np.zeros((nclasses, shape[0], shape[1]), dtype= 'float32')
    for i in range(labels.shape[0]):
        for c in range(nclasses):
            l= labels[i, :].reshape(shape)
            total_maps[c,:,:]+= (l==c)

    for i in range(shape[0]):
        for j in range(shape[1]):
            total_maps[:,i,j]= normalize(total_maps[:,i,j], nclasses)

    for c in range(nclasses):
        total_maps[c, :, :] = ndimage.gaussian_filter(total_maps[c, :, :], sigma=(11, 11), order=0)
        #plt.imshow(total_maps[c, :, :]); plt.show();

    return total_maps

def save_prior(maps):
    np.save('prior.npy', maps)

if __name__=="__main__":
    Y= read_traindata(sys.argv[1])
    prior_maps= compute_prior(Y, int(sys.argv[2]), [int(sys.argv[3]), int(sys.argv[4])])
    save_prior(prior_maps)
