import theano
import theano.tensor as T
import pdb
import numpy
from theano import config

from ..utils.generic import numpy_floatX

class SGD(object):

    """Docstring for SGD. """

    def __init__(self, learning_rate, momentum=0., lr_decay=1.):
        """TODO: to be defined1. """
        self.momentum = numpy_floatX(momentum)
        self.lr_shared = theano.shared(numpy_floatX(learning_rate),name='lr')
        self.lr_decay = numpy_floatX(lr_decay)
        self.lr_decay_shared = theano.shared(numpy_floatX(lr_decay),name='lr_decay')

    def get_update_func(self, lr_adj, tparams, grads, x, y, cost):
        """ Stochastic Gradient Descent

        :note: A more complicated version of sgd then needed.  This is
            done like that for adadelta and rmsprop.

        """
        momentum = self.momentum
        lr_decay= self.lr_decay

        gshared = [theano.shared(p.get_value() * numpy_floatX(0.), name='%s_grad' % k) for k, p in tparams.items()]
        gsup = [(gs, g) for gs, g in zip(gshared, grads)]

        f_grad_shared = theano.function([x, y], cost, updates=gsup, name='sgd_f_grad_shared')

        lr_up = [(self.lr_decay_shared,
                  self.lr_decay_shared*self.lr_decay)]

        vel = [theano.shared(p.get_value() * numpy_floatX(0.), name='%s_vel' % k) for k, p in tparams.items()]

        vel_up = [(v, self.momentum*v - self.lr_decay_shared * self.lr_shared * g*lr_adj[k]) for v,g,k in zip(vel, gshared, tparams.keys())]

        pup = [(p, p + v) for p, v in zip(tparams.values(), vel)]


        f_update = theano.function([], [], updates=vel_up + pup + lr_up, name='sgd_f_update')
        return f_grad_shared, f_update


class RMSPROP(object):

    """Docstring for RMSPROP. """

    def __init__(self):
        """TODO: to be defined1. """
        pass

    def get_update_func(self, lr_adj, tparams, grads, x, y, cost):
        """
        A variant of  SGD that scales the step size by running average of the
        recent step norms.

        Parameters
        ----------
        tpramas: Theano SharedVariable
            Model parameters
        grads: Theano variable
            Gradients of cost w.r.t to parameres
        x: Theano variable
            Model inputs
        mask: Theano variable
            Sequence mask
        y: Theano variable
            Targets
        cost: Theano variable
            Objective fucntion to minimize

        Notes
        -----
        For more information, see [Hint2014]_.

        .. [Hint2014] Geoff Hinton, *Neural Networks for Machine Learning*,
           lecture 6a,
           http://cs.toronto.edu/~tijmen/csc321/slides/lecture_slides_lec6.pdf
        """

        zipped_grads = [theano.shared(p.get_value() * numpy_floatX(0.),
                                      name='%s_grad' % k)
                        for k, p in tparams.items()]
        running_grads = [theano.shared(p.get_value() * numpy_floatX(0.),
                                       name='%s_rgrad' % k)
                         for k, p in tparams.items()]
        running_grads2 = [theano.shared(p.get_value() * numpy_floatX(0.),
                                        name='%s_rgrad2' % k)
                          for k, p in tparams.items()]

        zgup = [(zg, g) for zg, g in zip(zipped_grads, grads)]
        rgup = [(rg, 0.95 * rg + 0.05 * g) for rg, g in zip(running_grads, grads)]
        rg2up = [(rg2, 0.95 * rg2 + 0.05 * (g ** 2))
                 for rg2, g in zip(running_grads2, grads)]

        f_grad_shared = theano.function([x, y], cost,
                                        updates=zgup + rgup + rg2up,
                                        name='rmsprop_f_grad_shared')

        updir = [theano.shared(p.get_value() * numpy_floatX(0.),
                               name='%s_updir' % k)
                 for k, p in tparams.items()]
        updir_new = [(ud, 0.9 * ud - 1e-4 * zg / T.sqrt(rg2 - rg ** 2 + 1e-4))
                     for ud, zg, rg, rg2 in zip(updir, zipped_grads, running_grads,
                                                running_grads2)]
        param_up = [(p, p + udn[1])
                    for p, udn in zip(tparams.values(), updir_new)]
        f_update = theano.function([], [], updates=updir_new + param_up,
                                   on_unused_input='ignore',
                                   name='rmsprop_f_update')

        return f_grad_shared, f_update


class ADADELTA(object):

    """Docstring for ADADELTA. """

    def __init__(self,rho=0.95):
        """TODO: to be defined1. """
        self.rho = numpy_floatX(rho)

    def get_update_func(self, lr_adj, tparams, grads, x, y, cost):
        """
        An adaptive learning rate optimizer

        Parameters
        ----------
        rho : Theano SharedVariable
            Factor used in Running Avg
        tpramas: Theano SharedVariable
            Model parameters
        grads: Theano variable
            Gradients of cost w.r.t to parameres
        x: Theano variable
            Model inputs
        y: Theano variable
            Targets
        cost: Theano variable
            Objective fucntion to minimize

        Notes
        -----
        For more information, see [ADADELTA]_.

        .. [ADADELTA] Matthew D. Zeiler, *ADADELTA: An Adaptive Learning
           Rate Method*, arXiv:1212.5701.
        """
        zipped_grads = [theano.shared(p.get_value() * numpy_floatX(0.),
                                      name='%s_grad' % k)
                        for k, p in tparams.items()]
        running_up2 = [theano.shared(p.get_value() * numpy_floatX(0.),
                                     name='%s_rup2' % k)
                       for k, p in tparams.items()]
        running_grads2 = [theano.shared(p.get_value() * numpy_floatX(0.),
                                        name='%s_rgrad2' % k)
                          for k, p in tparams.items()]

        zgup = [(zg, g) for zg, g in zip(zipped_grads, grads)]
        rg2up = [(rg2, self.rho*rg2 + numpy_floatX(1.-self.rho)*(g ** 2))
                 for rg2, g in zip(running_grads2, grads)]

        f_grad_shared = theano.function([x, y], cost, updates=zgup + rg2up,
                                        name='adadelta_f_grad_shared')

        eps = numpy_floatX(1e-6)
        updir = [-T.sqrt(ru2 + eps) /T.sqrt(rg2 + eps) * zg
                 for zg, ru2, rg2 in zip(zipped_grads,
                                         running_up2,
                                         running_grads2)]
        ru2up = [(ru2, self.rho*ru2 + numpy_floatX(1.-self.rho)*(ud ** 2))
                 for ru2, ud in zip(running_up2, updir)]
        param_up = [(p, p + ud*lr_adj[k]) for p, ud, k in zip(tparams.values(), updir, tparams.keys())]

        f_update = theano.function([], [], updates=ru2up + param_up,
                                   on_unused_input='ignore',
                                   name='adadelta_f_update')

        return f_grad_shared, f_update

#def rmsprop(lr, lr_adj , tparams, grads, x, y, cost):
#    zipped_grads = [theano.shared(p.get_value() * numpy_floatX(0.),
#                                  name='%s_grad' % k)
#                    for k, p in tparams.items()]
#    running_grads2 = [theano.shared(p.get_value() * numpy_floatX(0.),
#                                    name='%s_rgrad2' % k)
#                      for k, p in tparams.items()]
#
#    zgup = [(zg, g) for zg, g in zip(zipped_grads, grads)]
#    rg2up = [(rg2, 0.95 * rg2 + 0.05 * (g ** 2))
#             for rg2, g in zip(running_grads2, grads)]
#
#    f_grad_shared = theano.function([x, y], cost, updates=zgup + rg2up,
#                                    name='rmsprop_f_grad_shared')
#
#    updir = [ -zg*(lr /T.sqrt(rg2 +1e-6))
#             for zg, rg2 in zip(zipped_grads,
#                                     running_grads2)]
#    param_up = [(p, p + ud*lr_adj[k]) for p, ud, k in zip(tparams.values(), updir, tparams.keys())]
#
#    f_update = theano.function([lr], [], updates=param_up,
#                               on_unused_input='ignore',
#                               name='rmsprop_f_update')
#    return f_grad_shared, f_update

