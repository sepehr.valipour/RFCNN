import gzip
import numpy
import matplotlib.pyplot as plt
from theano import config
import argparse
import pdb

import os
filepath = os.path.dirname(os.path.abspath(__file__))
rootpath = os.path.abspath(os.path.join(filepath, os.pardir))
os.sys.path.insert(0, filepath)
os.sys.path.insert(0, rootpath)

from rfcnn.core import Network
from rfcnn.core import SGD, RMSPROP, ADADELTA
from rfcnn.utils import numpy_floatX, load_all_datasets, load_weights, copy_caffe_weights
from rfcnn.layers import Conv, Deconv, Pooling, Relu, Data, Sigmoid, NLL, LRN, TransDeconv, SpatialDropout, Softmax, Skip, RectConv, DilatedConv
from rfcnn.core import TrainByBatch
from rfcnn.datahandle import DavisDataHandler
from rfcnn.visualization import Visualization
from rfcnn.datahandle import SynthiaHandler,CityScapesHandler

#directory= '/RFCN/camvid_all.h5'
#directory= '/RFCN/camvid_SEQ1_11_splits.h5'
#directory= '/RFCN/affordances_mugs.h5'
directory= '/RFCN/v4l.h5'
CHD = CityScapesHandler(directory)
#orig_size= [3,360, 480]
#nclasses = 12
#orig_size= [3,300, 300]
#nclasses = 4
orig_size= [3,480, 640]
nclasses = 8

# Camvid classes: 0: sky, 1: building, 2: pole, 3: road, 4: sidewalk, 5: vegetation, 6: traffic light, 7: car, 8: pedestrian, 9:
# Affordances classes: 0:void, 1: grasp, 2: wrap grasp, 3: contain
# V4l classes: 0:void, 1:screen, 2:keyboard, 3: devices, 4:ball&pillow (remove), 5:controller, 6:box, 7:tape

def prep_network():
    # Create the network layers
    net = Network(name='fcn')

    net.add_layer(Data('data', orig_size, reshape=True, mean_subtraction=(102.7170, 115.7726, 123.5094)))

    net.add_layer(Conv('conv1_1',3,1,3,64, padding=40, lr_adj=0.))
    net.add_layer(Relu('relu1_1'))
    net.add_layer(Conv('conv1_2',3,1,64,64, padding=1, lr_adj=0.))
    net.add_layer(Relu('relu1_2'))
    net.add_layer(Pooling('pool1'))

    net.add_layer(Conv('conv2_1',3,1,64,128,padding=1, lr_adj=0.))
    net.add_layer(Relu('relu2_1'))
    net.add_layer(Conv('conv2_2',3,1,128,128,padding=1, lr_adj=0.))
    net.add_layer(Relu('relu2_2'))

    net.add_layer(Pooling('pool2'))

    net.add_layer(Conv('conv3_1',3,1,128,256, padding=1))
    net.add_layer(Relu('relu3_1'))
    net.add_layer(Conv('conv3_2',3,1,256,256, padding=1))
    net.add_layer(Relu('relu3_2'))
    net.add_layer(Conv('conv3_3',3,1,256,256, padding=1))
    net.add_layer(Relu('relu3_3'))
    net.add_layer(Pooling('pool3'))

    net.add_layer(DilatedConv('dconv4',3,1, 256,256, 2, padding=1, lr_adj=1.))
    net.add_layer(Relu('relu4'))

    net.add_layer(DilatedConv('dconv5',3,1, 256,256, 4, padding=1, lr_adj=1.))
    net.add_layer(Relu('relu5'))

    net.add_layer(Conv('nconv6',1,1,256,512))
    net.add_layer(Relu('relu6'))

    net.add_layer(Conv('nconv7',1,1,512,128))
    net.add_layer(Relu('relu7'))

    net.add_layer(Skip('nskip1', ['relu7', 'relu3_3'], [2,1]))

    net.add_layer(Conv('nconv8',1,1,128+256,nclasses))

    net.add_layer(TransDeconv('deconv2', 4, 2, nclasses,
                              nclasses,
                              out_crop_size=(orig_size[1]//2, orig_size[2]//2)))

    net.add_layer(Softmax('prob'))
    net.add_layer(NLL('cost', 'segmentation'))
    net.optimizer = ADADELTA()
    net.create_functions(3, 2)
    pdb.set_trace()
    copy_caffe_weights(net, "/RFCN/VGG_Models/VGG_CNN_F.caffemodel",
                            "/RFCN/VGG_Models/VGG_deploy.prototxt")
    train_configs = {
                'max_epochs' : 500,  # The maximum number of epoch to run
                'valid_freq' : 10,  # Compute the validation error after this number of update.
                'patience' : 400,  # Number of epoch to wait before early stop if no progress
                'save_freq': 10,
                'train_batch_size': 2,  # The batch size during training.
                'valid_batch_size': 1,  # The batch size during training.
                'net_save_path': "/RFCN/RFCNN_data/models/vgg_fcn16s_v4l_2.save",
                'log_file' : '/RFCN/RFCNN_data/logs/vgg_fcn16s_v4l_2.txt',
                }
    train = TrainByBatch(net, datahandler=CHD,
                  configs=train_configs, nclasses= nclasses)
    return train

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('mode', help="choose either test or train. if test, provide the path to the model (--model)\n \
                        add train along with a model to continue training.")
    parser.add_argument('--model', help="path to the model(.save)")
    parser.add_argument('--output', help="Output Directory")
    args = parser.parse_args()

    train = prep_network()
    if args.mode == 'train':
        if args.model is not None:
            print ("continue training from %s"%(args.model))
            load_weights(train.net, args.model)
        train.run_all()

    elif args.mode == 'test':
        if args.model is None:
            raise NameError("no model is passed for testing")

        load_weights(train.net, args.model)
        met,_ = train.eval('valid')
        print ("cost = %s, precision = %s, recall = %s, fmeasure =%s, Mean IOU= %s, IOU= %s"%(
               met.cost, met.prec, met.rec, met.fmes, met.mean_iou_index, met.iou))

    elif args.mode == 'visualize':
        load_weights(train.net, args.model)
        vis= Visualization(train.net, (orig_size[1], orig_size[2]), data_handle= CHD, out_dir=args.output, num_classes=nclasses, n_frames= 1)
        vis.generate_vis_data(CHD.hf['images_valid'], CHD.hf['labels_valid'])

if __name__ == '__main__':
        main()
