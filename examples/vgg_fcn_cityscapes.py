import gzip
import numpy
import matplotlib.pyplot as plt
from theano import config
import argparse
import pdb

import os
filepath = os.path.dirname(os.path.abspath(__file__))
rootpath = os.path.abspath(os.path.join(filepath, os.pardir))
os.sys.path.insert(0, filepath)
os.sys.path.insert(0, rootpath)

from rfcnn.core import Network
from rfcnn.core import SGD, RMSPROP, ADADELTA
from rfcnn.utils import numpy_floatX, load_all_datasets, load_weights, copy_caffe_weights
from rfcnn.layers import Conv, Deconv, Pooling, Relu, Data, Sigmoid, NLL, LRN, TransDeconv, SpatialDropout, Softmax, Skip
from rfcnn.core import TrainByBatch
from rfcnn.datahandle import DavisDataHandler
from rfcnn.visualization import Visualization
from rfcnn.datahandle import CityScapesHandler

directory= '/usr/data/Datasets/cityscapes/leftImg8bit.h5'
CHD = CityScapesHandler(directory)
orig_size= [3, 256, 512]
nclasses = 34

def prep_network():
    # Create the network layers
    net = Network(name='fcn')

    net.add_layer(Data('data', orig_size, reshape=True, mean_subtraction=(102.7170, 115.7726, 123.5094)))

    net.add_layer(Conv('conv1',11,4,3,64, padding=40, lr_adj=1.))
    net.add_layer(Relu('relu1'))
    net.add_layer(Pooling('pool1'))
    net.add_layer(LRN('LRN1'))

    net.add_layer(Conv('conv2',5,1,64,256,padding=2, lr_adj=1.))
    net.add_layer(Relu('relu2'))
    net.add_layer(Pooling('pool2'))

    net.add_layer(Conv('conv3',3,1,256,256, padding=1, lr_adj=1.))
    net.add_layer(Relu('relu3'))

    net.add_layer(Conv('conv4',3,1,256,256, padding=1, lr_adj=1.))
    net.add_layer(Relu('relu4'))

    net.add_layer(Conv('conv5',3,1,256,256, padding=1, lr_adj=1.))
    net.add_layer(Relu('relu5'))

    net.add_layer(Conv('conv6',1,1,256,512))
    net.add_layer(Relu('relu6'))

    net.add_layer(Conv('conv7',1,1,512,128))
    net.add_layer(Relu('relu7'))

    net.add_layer(Conv('conv8',1,1,128,nclasses))

    net.add_layer(TransDeconv('deconv1', 20, 8, nclasses,
                              nclasses,
                              out_crop_size=(orig_size[1]//2, orig_size[2]//2)))

    net.add_layer(Softmax('prob'))
    net.add_layer(NLL('cost', 'segmentation'))

    copy_caffe_weights(net, "/usr/data/RFCNN_data/VGG_Models/VGG_CNN_F.caffemodel",
                            "/usr/data/RFCNN_data/VGG_Models/VGG_deploy.prototxt")
    net.optimizer = ADADELTA()
    net.create_functions(3, 2)
    train_configs = {
                'max_epochs' : 500,  # The maximum number of epoch to run
                'valid_freq' : 2,  # Compute the validation error after this number of update.
                'patience' : 400,  # Number of epoch to wait before early stop if no progress
                'save_freq': 10,
                'train_batch_size': 10,  # The batch size during training.
                'net_save_path': "/usr/data/menna/RFCNN_data/models/vgg_fcn_cityscapes_half.save",
                'log_file' : '/usr/data/menna/RFCNN_data/logs/vgg_fcn_cityscapes_half.txt',
                }
    train = TrainByBatch(net, datahandler=CHD,
                  configs=train_configs, nclasses= nclasses)
    return train

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('mode', help="choose either test or train. if test, provide the path to the model (--model)\n \
                        add train along with a model to continue training.")
    parser.add_argument('--model', help="path to the model(.save)")
    parser.add_argument('--output', help="Output Directory")
    args = parser.parse_args()

    train = prep_network()
    if args.mode == 'train':
        if args.model is not None:
            print ("continue training from %s"%(args.model))
            load_weights(train.net, args.model)
        train.run_all()

    elif args.mode == 'test':
        if args.model is None:
            raise NameError("no model is passed for testing")

        load_weights(train.net, args.model)
        met,_ = train.eval('valid')
        print ("cost = %s, precision = %s, recall = %s, fmeasure =%s, Mean IOU= %s, IOU= %s"%(
               met.cost, met.prec, met.rec, met.fmes, met.mean_iou_index, met.iou))

if __name__ == '__main__':
        main()
