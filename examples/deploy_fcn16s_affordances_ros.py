#!/usr/bin/python2.7
import gzip
import numpy
import matplotlib.pyplot as plt
from theano import config
import argparse
import pdb
import scipy.misc
import cv2

import Image
import os
filepath = os.path.dirname(os.path.abspath(__file__))
rootpath = os.path.abspath(os.path.join(filepath, os.pardir))
os.sys.path.insert(0, filepath)
os.sys.path.insert(0, rootpath)

from rfcnn.core import Network
from rfcnn.core import SGD, RMSPROP, ADADELTA
from rfcnn.utils import numpy_floatX, load_all_datasets, load_weights_cpu, copy_caffe_weights
from rfcnn.layers import Conv, Deconv, Pooling, Relu, Data, Sigmoid, NLL, LRN, TransDeconv, SpatialDropout, Softmax, Skip, RectConv, DilatedConv
from rfcnn.core import TrainByBatch
from rfcnn.datahandle import DavisDataHandler
from rfcnn.visualization import Visualization
from rfcnn.datahandle import SynthiaHandler,CityScapesHandler

from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

directory= '/usr/data/Datasets/Affordances/affordances_mugs.h5'
CHD = CityScapesHandler(directory)
#orig_size= [3,360, 480]
#nclasses = 12
orig_size= [3,300, 300]
nclasses = 4

import rospy
from std_msgs.msg import String

train= None
bridge = CvBridge()

def callback(data):
    try:
        img = bridge.imgmsg_to_cv2(data, "bgr8")
    except CvBridgeError as e:
        print(e)

    if train is not None:
        img= scipy.misc.imresize(img[:,:,::-1].copy(), (300,300))
        image= img.reshape((-1,3)).transpose(1,0)
        image= numpy.expand_dims(image, axis=0)
        out= train.forward(image).reshape((orig_size[1]//2, orig_size[2]//2))
        out2= scipy.misc.imresize(numpy.array(out,dtype='uint8'),orig_size[1:])
        labels_of_interest= [3]#,2]
        labels= ['contain']#, 'w-grasp']
        counter = 0
        img_cpy= img[:,:,::-1].copy()

        for l in labels_of_interest:
            out2_cpy= out2.copy()
            out2_cpy[out2!=l]= 0
            out2_cpy[out2==l]= 255
            out2_cpy, cx, cy= postprocess(out2_cpy)
            if l==3:
                ccx= cx; ccy= cy
            else:
                d= numpy.sqrt((ccx-cx)**2+(ccy-cy)**2)
                if d> 40:
                    continue
            cv2.circle(img_cpy, (cx,cy),  5, (255,0,0), thickness=2)
            cv2.putText(img_cpy, labels[counter],(cx+5,cy+5),  cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,255,255), 1)
            counter += 1
        cv2.imshow('Affordances',img_cpy)
        cv2.waitKey(1)

def listener():
    rospy.init_node('affordances', anonymous=True)
    rospy.Subscriber("/camera/rgb/image_rect_color", Image, callback)
    rospy.spin()

def prep_network():
    # Create the network layers
    net = Network(name='fcn')

    net.add_layer(Data('data', orig_size, reshape=True, mean_subtraction=(102.7170, 115.7726, 123.5094)))

    net.add_layer(Conv('conv1_1',3,1,3,64, padding=40, lr_adj=0.))
    net.add_layer(Relu('relu1_1'))
    net.add_layer(Conv('conv1_2',3,1,64,64, padding=1, lr_adj=0.))
    net.add_layer(Relu('relu1_2'))
    net.add_layer(Pooling('pool1'))

    net.add_layer(Conv('conv2_1',3,1,64,128,padding=1, lr_adj=0.))
    net.add_layer(Relu('relu2_1'))
    net.add_layer(Conv('conv2_2',3,1,128,128,padding=1, lr_adj=0.))
    net.add_layer(Relu('relu2_2'))

    net.add_layer(Pooling('pool2'))

    net.add_layer(Conv('conv3_1',3,1,128,256, padding=1))
    net.add_layer(Relu('relu3_1'))
    net.add_layer(Conv('conv3_2',3,1,256,256, padding=1))
    net.add_layer(Relu('relu3_2'))
    net.add_layer(Conv('conv3_3',3,1,256,256, padding=1))
    net.add_layer(Relu('relu3_3'))
    net.add_layer(Pooling('pool3'))

    net.add_layer(DilatedConv('dconv4',3,1, 256,256, 2, padding=1, lr_adj=1.))
    net.add_layer(Relu('relu4'))

    net.add_layer(DilatedConv('dconv5',3,1, 256,256, 4, padding=1, lr_adj=1.))
    net.add_layer(Relu('relu5'))

    net.add_layer(Conv('nconv6',1,1,256,512))
    net.add_layer(Relu('relu6'))

    net.add_layer(Conv('nconv7',1,1,512,128))
    net.add_layer(Relu('relu7'))

    net.add_layer(Skip('nskip1', ['relu7', 'relu3_3'], [2,1]))

    net.add_layer(Conv('nconv8',1,1,128+256,nclasses))

    net.add_layer(TransDeconv('deconv2', 4, 2, nclasses,
                              nclasses,
                              out_crop_size=(orig_size[1]//2, orig_size[2]//2)))

    net.add_layer(Softmax('prob'))
    net.add_layer(NLL('cost', 'segmentation'))
    net.optimizer = ADADELTA()
    net.create_functions(3, 2)
    train_configs = {
                'max_epochs' : 500,  # The maximum number of epoch to run
                'valid_freq' : 10,  # Compute the validation error after this number of update.
                'patience' : 400,  # Number of epoch to wait before early stop if no progress
                'save_freq': 10,
                'train_batch_size': 1,  # The batch size during training.
                'valid_batch_size': 1,  # The batch size during training.
                }
    train = TrainByBatch(net, datahandler=CHD,
                  configs=train_configs, nclasses= nclasses)
    return train

def roi_crop(img, size):
    cx= img.shape[0]//2
    cy= img.shape[1]//2
    if len(img.shape)==3:
        crop= img[cx-size[0]//2:cx+size[0]//2, cy-size[1]//2:cy+size[1]//2,:]
    else:
        crop= img[cx-size[0]//2:cx+size[0]//2, cy-size[1]//2:cy+size[1]//2]
    return crop

def create_overlay_2(im, mask):
        orig_shape = im.shape
        im = Image.fromarray(numpy.uint8(im))

        colors= [(0,0,255), (255,0,0), (0, 128, 0)]
        mask_r= numpy.zeros((mask.shape[0], mask.shape[1],3), dtype= numpy.uint8)
        interest= [3]
        for i in range(0,len(interest)):
            for j in range(3):
                mask_r[(mask==interest[i]),j]= colors[i][j]
        overlay = Image.fromarray(numpy.uint8(mask_r))
        overlay = overlay.convert("RGBA")

        im = im.convert("RGBA")
        im = Image.blend(im, overlay, 0.4)
        return im


def create_overlay(im, mask):
        orig_shape = im.shape
        im = Image.fromarray(numpy.uint8(im))
        overlay = Image.fromarray(numpy.uint8(mask))
        overlay = overlay.convert("RGBA")
        im = im.convert("RGBA")
        im = Image.blend(im, overlay, 0.4)
        return im

def postprocess(out):
    kernel = numpy.ones((5,5), numpy.uint8)
    mask= cv2.dilate(out, kernel, iterations=1)
    contours, hierarchy = cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE )
    maxContour = 0
    for contour in contours:
        contourSize = cv2.contourArea(contour)
        if contourSize > maxContour:
            maxContourData = contour
            maxContour= contourSize
    mask_f = numpy.zeros_like(mask)
    if maxContour==0:
        return mask_f,-50,-50
    cv2.fillPoly(mask_f,[maxContourData],1)
    mask_f[mask_f==1]=255
    M= cv2.moments(maxContourData)
    cX = int(M["m10"] / M["m00"])
    cY = int(M["m01"] / M["m00"])
    return mask_f, cX, cY

if __name__ == '__main__':
    train = prep_network()
    load_weights_cpu(train.net, '/usr/data/menna/RFCNN_data/models/vgg_fcn16s_affordances_mug.save')
    listener()


