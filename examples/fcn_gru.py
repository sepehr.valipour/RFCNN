import gzip
import numpy
import matplotlib.pyplot as plt
from theano import config
import argparse

import os
parentpath = os.path.dirname(os.path.abspath(__file__))
os.sys.path.insert(0, parentpath)
from rfcnn.core import Network
from rfcnn.core import SGD, ADADELTA
from rfcnn.utils import load_all_datasets, numpy_floatX, sliding_window
from rfcnn.layers import Conv, Deconv, Pooling, Relu, Data, Sigmoid, NLL, GRU
from rfcnn.core import Train


# Loading dataset from pedestrians
directory= '/usr/data/Datasets/changedetection/numpy_data/'
dataset_names= ['pedestrians.npy']

train_set_x, train_set_y, valid_set_x, valid_set_y, test_set_x, test_set_y= load_all_datasets(dataset_names, directory, db_type= 'npy')
orig_size= [3, 120, 180]

train_set_y= numpy.asarray(train_set_y, dtype='int64' )
train_set_x= numpy_floatX(train_set_x/255.0)
valid_set_y= numpy.asarray(valid_set_y, dtype='int64' )
valid_set_x= numpy_floatX(valid_set_x/255.0)

nframes=3
train_set_x, train_set_y= sliding_window(train_set_x, train_set_y, nframes)
train_set_y = numpy.ndarray.astype(train_set_y[:,-1,:]>0,numpy.int64)
valid_set_x, valid_set_y= sliding_window(valid_set_x, valid_set_y, nframes)
valid_set_y = numpy.ndarray.astype(valid_set_y[:,-1,:]>0,numpy.int64)

tr_ind= numpy.random.permutation(train_set_x.shape[0])
train_set_x = train_set_x[tr_ind, :]
train_set_y = train_set_y[tr_ind, :]

def prep_network():
    # Create the network layers
    net = Network(name='fcn')

    int1 = Network(name='gru_int')

    int1.add_layer(Data('data', orig_size, reshape=True))
    int1.add_layer(Conv('conv1',5,3,3,20, padding=50))
    int1.add_layer(Relu('relu1'))
    int1.add_layer(Pooling('pool1'))

    int1.add_layer(Conv('conv2',5,1,20,50))
    int1.add_layer(Relu('relu2'))
    int1.add_layer(Pooling('pool2'))

    int1.add_layer(Conv('conv3',4,1,50,500))
    int1.add_layer(Relu('relu3'))

    int1.add_layer(Conv('conv4',1,1,500,1))

    net.add_layer(GRU('gru',int1))

    net.add_layer(Deconv('deconv1', 20, 12, 1, 1, out_crop_size = (orig_size[1], orig_size[2])))

    net.add_layer(Sigmoid('prob'))

    net.add_layer(NLL('cost', 'segmentation'))

    net.optimizer = ADADELTA()
    net.create_functions(4, 2)
    train_configs = {
                'max_epochs' : 500,  # The maximum number of epoch to run
                'train_disp_freq' : 100,  # Display to stdout the training progress every N updates
                'valid_freq' : 10,  # Compute the validation error after this number of update.
                'train_batch_size':16,  # The batch size during training.
                'net_save_path': "/usr/data/RFCNN_data/models/fcn_gru.save",
                'log_file' : '/usr/data/RFCNN_data/logs/fcn_gru.txt',
                }

    train = Train(net, train_set_x, train_set_y,
                  X_valid=valid_set_x, Y_valid=valid_set_y,
                  configs=train_configs)
    return train

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('mode', help="choose either test or train. if test, provide the path to the model (--model)\n \
                        add train along with a model to continue training.")
    parser.add_argument('--model', help="path to the model(.save)")
    args = parser.parse_args()

    train = prep_network()
    if args.mode == 'train':
        if args.model is not None:
            print ("continue training from %s"%(args.model))
            load_weights(train.net, args.model)
        train.run_all()

    elif args.mode == 'test':
        if args.model is None:
            raise NameError("no model is passed for testing")
        load_weights(train.net, args.model)
        cost, prec, recall, fmeasure = train.eval(valid_set_x, valid_set_y)
        print ("cost = %s, precision = %s, recall = %s, fmeasure =%s"%(
               cost, prec, recall, fmeasure))

if __name__ == '__main__':
        main()
