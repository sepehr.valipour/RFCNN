import gzip
import numpy
import matplotlib.pyplot as plt
from theano import config
import argparse
import pdb

import os
parentpath = os.path.dirname(os.path.abspath(__file__))
os.sys.path.insert(0, parentpath)
from rfcnn.core import Network
from rfcnn.core import SGD, RMSPROP, ADADELTA
from rfcnn.utils import numpy_floatX, load_all_datasets, load_weights, copy_caffe_weights, sliding_window
from rfcnn.layers import Conv, Deconv, Pooling, Relu, Data, Sigmoid, NLL, LRN, TransDeconv, ConvGRU
from rfcnn.core import Train
from rfcnn.datahandle import ChangeDetectionDataHandler, DavisDataHandler, SegTrackHandler


# Loading dataset from pedestrians

#directory= '/usr/data/Datasets/changedetection_datasets/numpy_data/'
#directory= '/usr/data/Datasets/changedetection/numpy_data_2/'
#dataset_names= ['pedestrians'] #, 'PETS2006', 'badminton', 'copyMachine']#, 'office', 'sofa']
#train_set_x, _, valid_set_x, _, test_set_x, _ = load_all_datasets(dataset_names, directory, load='data')
#
#directory= '/usr/data/Datasets/changedetection/numpy_data/'
#dataset_names= ['pedestrians.npy'] #, 'PETS2006.npy', 'badminton.npy', 'copyMachine.npy']#, 'office.npy', 'sofa.npy']
#_, train_set_y, _, valid_set_y, _, test_set_y = load_all_datasets(dataset_names, directory, db_type='npy', load='labels')

#directory= '/usr/data/menna/Datasets/change_detection_datasets/numpy_data/'
#dataset_names= ['pedestrians.npy']#, 'PETS2006.npy', 'badminton.npy', 'copyMachine.npy']
#train_set_x, train_set_y, valid_set_x, valid_set_y,test_set_x, test_set_y= load_all_datasets(dataset_names, directory, db_type='npy')
#
#directory= '/usr/data/Datasets/changedetection/numpy_data_2/'
#dataset_names= ['pedestrians']
#train_set_x, train_set_y1, valid_set_x, valid_set_y1,test_set_x, test_set_y1= load_all_datasets(dataset_names, directory)

#orig_size= [3, 240, 360]
#
#train_set_y= numpy.asarray(train_set_y, dtype='int64' )
#train_set_x= numpy_floatX(train_set_x)
#valid_set_y= numpy.asarray(valid_set_y, dtype='int64' )
#valid_set_x= numpy_floatX(valid_set_x)
#test_set_y= numpy.asarray(test_set_y, dtype='int64' )
#test_set_x= numpy_floatX(test_set_x)
#
#nframes=3
#train_set_x, train_set_y= sliding_window(train_set_x, train_set_y, nframes)
#train_set_y = numpy.ndarray.astype(train_set_y[:,-1,:]>0,numpy.int64)
#valid_set_x, valid_set_y= sliding_window(valid_set_x, valid_set_y, nframes)
#valid_set_y = numpy.ndarray.astype(valid_set_y[:,-1,:]>0,numpy.int64)
#
#
#tr_ind= numpy.random.permutation(train_set_x.shape[0])
#train_set_x = train_set_x[tr_ind, :]
#train_set_y = train_set_y[tr_ind, :]

directory= '/usr/data/Datasets/SegTrackv2/data_1/'
CHD = SegTrackHandler(directory, temporal=True, n_frames=3)

orig_size= [3, 240, 360]
def prep_network():
    # Create the network layers
    net = Network(name='fcn')

    int1 = Network(name='convgru_int')
    int1.add_layer(Data('data', orig_size, reshape=True, mean_subtraction=(102.7170, 115.7726, 123.5094)))

    int1.add_layer(Conv('conv1',11,4,3,64, padding=40, lr_adj=0.))
    int1.add_layer(Relu('relu1'))
    int1.add_layer(Pooling('pool1'))
    int1.add_layer(LRN('LRN1'))

    int1.add_layer(Conv('conv2',5,1,64,256,padding=2, lr_adj=0.))
    int1.add_layer(Relu('relu2'))
    int1.add_layer(Pooling('pool2'))

    int1.add_layer(Conv('conv3',3,1,256,256, padding=1, lr_adj=0.))
    int1.add_layer(Relu('relu3'))

    int1.add_layer(Conv('conv4',3,1,256,256, padding=1, lr_adj=0.))
    int1.add_layer(Relu('relu4'))

    int1.add_layer(Conv('conv5',3,1,256,256, padding=1, lr_adj=0.))
    int1.add_layer(Relu('relu5'))

    int1.add_layer(Conv('conv6',1,1,256,512, lr_adj=0.05))
    int1.add_layer(Relu('relu6'))

    int1.add_layer(Conv('conv7',1,1,512,128, lr_adj=0.05))
    int1.add_layer(Relu('relu7'))

    int1.add_layer(Conv('conv8',1,1,128,1, lr_adj=0.05))

    net.add_layer(ConvGRU('convgru', int1, 3, 1, 1, 1, pad=3))

    net.add_layer(TransDeconv('deconv1', 20, 8, 1, 1, out_crop_size = (orig_size[1]//2, orig_size[2]//2)))

    net.add_layer(Sigmoid('prob'))
    net.add_layer(NLL('cost', 'segmentation'))

    copy_caffe_weights(net, "/usr/data/RFCNN_data/VGG_Models/VGG_CNN_F.caffemodel",
                            "/usr/data/RFCNN_data/VGG_Models/VGG_deploy.prototxt")

    net.optimizer = ADADELTA()
    net.create_functions(4, 2)
    train_configs = {
                'max_epochs' : 1000,  # The maximum number of epoch to run
                'valid_freq' : 2,  # Compute the validation error after this number of update.
                'save_freq': 10,
                'train_batch_size': 2,  # The batch size during training.
                #'net_save_path': "/usr/data/RFCNN_data/models/vgg_rfcn.save",
                'log_file' : '/usr/data/RFCNN_data/logs/vgg_rfcn_segtrack.txt',
                }

    train = Train(net, datahandler=CHD,
                  X_valid=CHD.valid_x, Y_valid=CHD.valid_y,
                  configs=train_configs)
    return train

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('mode', help="choose either test or train. if test, provide the path to the model (--model)\n \
                        add train along with a model to continue training.")
    parser.add_argument('--modelfcn', help="path to the model(.save)")
    parser.add_argument('--modelgru', help="path to the model(.save)")
    args = parser.parse_args()

    train = prep_network()
    if args.mode == 'train':
        if args.modelfcn is not None:
            print ("continue training from %s"%(args.modelfcn))
            load_weights(train.net, args.modelfcn)
        if args.modelgru is not None:
            print ("continue training from %s"%(args.modelgru))
            load_weights(train.net, args.modelgru)

        train.run_all()

    elif args.mode == 'test':
        if args.model is None:
            raise NameError("no model is passed for testing")

        load_weights(train.net, args.model)
        cost, prec, recall, fmeasure = train.eval(valid_set_x, valid_set_y)
        print ("cost = %s, precision = %s, recall = %s, fmeasure =%s"%(
               cost, prec, recall, fmeasure))

if __name__ == '__main__':
        main()
